# install conda in /root

class condamirror::conda {
	
	$conda_dir = '/root/miniforge'

	exec{'download_miniforge':
		command => '/usr/bin/curl -L -O https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-Linux-x86_64.sh',
		cwd => '/root',
		creates => '/root/Miniforge3-Linux-x86_64.sh',
		}
		
	exec{'install_miniforge':
		command => "/bin/bash /root/Miniforge3-Linux-x86_64.sh -b -p ${conda_dir}",
		creates => $conda_dir,
		cwd => '/root',
		subscribe => Exec['download_miniforge'],
		}
		
	exec{'install_conda-build':
		command => "${conda_dir}/bin/conda install conda-build",
		refreshonly => true,
		subscribe => Exec['install_miniforge'],
		}
}
