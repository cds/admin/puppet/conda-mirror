# install conda channel mirror server

class condamirror(
	String $on_calendar = "*-*-* 3:15:00"
)
{
	include condamirror::conda

	# install some needed packages
	package{ ['git', 'python3-venv', 'apt-transport-https', 'ca-certificates', 'apache2',
	]: ensure=>installed, }

	# create source directory
	$src_base = '/root/source'
	file{$src_base: ensure => directory }
	
	$condamirror = lookup('conda_mirror')
	
	$src_url = $condamirror['mirror_source_url']

	$src_dir = "${src_base}/conda-mirror"
	
	$conda_dir = "/root/miniforge"
	
	$hostname = $facts['networking']['hostname']
	
	$channel_url = "http://$hostname/conda"


	# clone and install source locally
	exec {'download_mirror_source':
		command => "/usr/bin/git clone ${src_url}",
		cwd => $src_base,
		creates => $src_dir, 
	}

	exec {'install_mirror_source':
		command => "$src_dir/install_mirror_source.sh",
		cwd => $src_dir,
		refreshonly => true,
		subscribe => Exec['download_mirror_source']
	}
	
	
	
	# create systemd unit files
	$channel = $condamirror['channel']
	
	$env_list_url = $condamirror['environment_list_url']

	$repo_dir = $condamirror['repo_dir']

	file{$repo_dir: ensure => directory, }
	
	$systemd_path = '/usr/local/lib/systemd/system'
	
	file {'/usr/local/lib/systemd': ensure => directory, }
	file {$systemd_path: ensure => directory, } 

	file {"${systemd_path}/conda-mirror.service":
		content => "[Unit]
Description=Mirror part of $channel

[Service]
ExecStart=$src_dir/startmirror.sh $src_dir $env_list_url $channel $repo_dir $conda_dir $channel_url
",
	}
	
	file {"${systemd_path}/conda-mirror.timer":
		content => @("EOF"/L)
			# This file is managed by puppet
			[Unit]
			Description=Update conda mirror daily

			[Timer]
			OnCalendar=${on_calendar}

			[Install]
			WantedBy=timers.target
			| EOF
}

	exec {'/usr/bin/systemctl daemon-reload':
		refreshonly => true,
		subscribe => [File["${systemd_path}/conda-mirror.timer"], File["${systemd_path}/conda-mirror.service"]],
	}
	
	service {'conda-mirror.timer':
		ensure => running,
		enable => true,
	}
	Exec['/usr/bin/systemctl daemon-reload'] -> Service['conda-mirror.timer']

}
